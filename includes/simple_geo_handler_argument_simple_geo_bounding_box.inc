<?php
class simple_geo_handler_argument_simple_geo_bounding_box extends views_handler_argument_string {
  function query() {
    $this->ensure_my_table();

    if (!empty($this->options['break_phrase'])) {
      views_break_phrase($this->argument, $this);
    }
    else {
      $this->value = array($this->argument);
    }
    $pos = explode(',', $this->value[0]);
    if (count($pos) == 4) {
      // Make sure all values are floats so they can be safely insterted in
      // the SQL.
      $pos = array_map("floatval", $pos);
      $this->query->add_where(0, "Contains(Envelope(GeomFromText('LineString($pos[0] $pos[1],$pos[2] $pos[3])')), $this->table_alias.$this->real_field)");
    }
  }
}
